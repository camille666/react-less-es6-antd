# 管理后台脚手架

不涉及状态管理。

## 一、预览

![预览效果](./doc/result.gif)


## 三、运行

1、安装依赖

````
yarn install
````

2、启动服务端

````
npm run server
````

3、启动客户端

````
npm run dev
````


babel-plugin-import实现antd的按需加载