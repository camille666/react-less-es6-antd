module.exports = {
    success: true,
    code: 0,
    msg: '查询成功',
    data: {
        type: 'feedbackoption',
        code: 'risk',
        result: [
            {
                value: '持续关注',
                order: '1'
            },
            {
                value: '行政处罚',
                order: '2'
            },
            {
                value: '强制措施',
                order: '3'
            },
            {
                value: '其他',
                order: '4'
            }
        ]
    }
}
