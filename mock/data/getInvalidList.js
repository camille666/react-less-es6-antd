module.exports = {
    success: true,
    code: 0,
    msg: '查询成功',
    data: {
        type: 'feedbackoption',
        code: 'invalid',
        result: [
            {
                value: '未找到此人',
                order: '1'
            },
            {
                value: '非工作对象',
                order: '2'
            },
            {
                value: '其他',
                order: '3'
            }
        ]
    }
}
