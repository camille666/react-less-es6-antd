module.exports = {
    success: true,
    code: 0,
    msg: '查询成功',
    data: {
        type: 'feedbackoption',
        code: 'transport',
        result: [
            {
                value: '火车',
                order: '1'
            },
            {
                value: '飞机',
                order: '2'
            },
            {
                value: '大巴',
                order: '3'
            },
            {
                value: '私家车',
                order: '4'
            },
            {
                value: '其他',
                order: '5'
            }
        ]
    }
}
