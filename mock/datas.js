const getResult = require('./data/getResult')
const getInvalidList = require('./data/getInvalidList')
const getRiskList = require('./data/getRiskList')
const getTransportList = require('./data/getTransportList')

module.exports = {
    getResult,
    getInvalidList,
    getRiskList,
    getTransportList
}
