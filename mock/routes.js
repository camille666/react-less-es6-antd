module.exports = {
    '/api/feedback*': '/getResult',
    '/api/getInvalidList*': '/getInvalidList',
    '/api/getRiskList*': '/getRiskList',
    '/api/getTransportList*': '/getTransportList'
}
