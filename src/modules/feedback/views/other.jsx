import React from 'react'
import { Form, Select, Radio } from 'antd'

import styles from './other.less'
const FormItem = Form.Item
const Option = Select.Option
const Group = Radio.Group

class Other extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            resultList: this.props.resultList
        }
    }

    // 单选值改变时
    handleRadioChange(type, e) {
        const { isValid, isRisk } = this.props
        const { formObj } = this.props
        const { setFieldsValue } = formObj
        if ((isValid && isRisk) || !isValid) {
            if (type === 'valid') {
                setFieldsValue({ conclusion: '' })
            }
        }

        this.props.changeRadio(type, e)
    }

    static getDerivedStateFromProps(props, state) {
        // 有效，有风险
        if (
            JSON.stringify(props.resultList) !==
            JSON.stringify(state.resultList)
        ) {
            if (props.resultList) {
                return {
                    resultList: props.resultList || []
                }
            } else {
                return null
            }
        } else {
            return null
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (
            JSON.stringify(this.props.resultList) !==
            JSON.stringify(this.state.resultList)
        ) {
            if (this.props.resultList) {
                this.setState({
                    resultList: this.props.resultList || []
                })
            }
        }
    }

    render() {
        const { isValid, isRisk } = this.props
        const { resultList } = this.state
        const { formObj } = this.props

        const { getFieldDecorator } = formObj

        const formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 9 }
        }
        const renderDom = (
            <div className={styles.other}>
                <FormItem
                    {...formItemLayout}
                    colon={false}
                    label="是否为有效线索"
                >
                    {getFieldDecorator('valid', {
                        initialValue: 1
                    })(
                        <Group
                            onChange={this.handleRadioChange.bind(
                                this,
                                'valid'
                            )}
                        >
                            <Radio value={1}> 有效 </Radio>
                            <Radio value={0}> 无效 </Radio>
                        </Group>
                    )}
                </FormItem>
                {isValid ? (
                    <FormItem
                        {...formItemLayout}
                        colon={false}
                        label="是否有风险"
                    >
                        {getFieldDecorator('risk', {
                            initialValue: 1
                        })(
                            <Group
                                onChange={this.handleRadioChange.bind(
                                    this,
                                    'risk'
                                )}
                            >
                                <Radio value={1} style={{ width: 60 }}>
                                    是
                                </Radio>
                                <Radio value={0}>否</Radio>
                            </Group>
                        )}
                    </FormItem>
                ) : (
                    ''
                )}

                {(isValid && isRisk) || !isValid ? (
                    <FormItem
                        {...formItemLayout}
                        colon={false}
                        label="反馈结论"
                    >
                        {getFieldDecorator('conclusion', {
                            initialValue: ''
                        })(
                            <Select
                                id="J_other"
                                allowClear={true}
                                placeholder="请选择"
                            >
                                {resultList &&
                                    resultList.map((item, index) => {
                                        return (
                                            <Option
                                                key={`conclusion${index}`}
                                                value={item.value}
                                            >
                                                {item.value}
                                            </Option>
                                        )
                                    })}
                            </Select>
                        )}
                    </FormItem>
                ) : (
                    ''
                )}
            </div>
        )
        return renderDom
    }
}

export default Other
