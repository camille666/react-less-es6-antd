import React from 'react'
import { Form, Row, Col, Input } from 'antd'
const FormItem = Form.Item

class FeedBack extends React.Component {
    // 输入值改变时
    handleInputChange(type, e) {
        // console.log(type, e);
        this.setState({
            [type]: e.target.value
        })
    }

    render() {
        const { formObj } = this.props
        const { getFieldDecorator } = formObj

        const twoCol = {
            labelCol: { span: 5 },
            wrapperCol: { span: 19 }
        }

        const oneCol = {
            labelCol: { span: 2 },
            wrapperCol: { span: 22 }
        }

        const renderDom = (
            <div>
                <Row>
                    <Col span={12}>
                        <FormItem
                            colon={false}
                            label="国籍"
                            labelCol={{ span: 3 }}
                            wrapperCol={{ span: 18 }}
                            style={{ marginLeft: 21 }}
                        >
                            {getFieldDecorator('nation')(
                                <Input
                                    type="text"
                                    placeholder="请输入"
                                    autoComplete="off"
                                    onChange={this.handleInputChange.bind(
                                        this,
                                        'nation'
                                    )}
                                    maxLength={32}
                                    style={{ width: 287, marginLeft: 5 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                    <Col span={12}>
                        <FormItem colon={false} label="护照号" {...twoCol}>
                            {getFieldDecorator('passport')(
                                <Input
                                    type="text"
                                    placeholder="请输入"
                                    autoComplete="off"
                                    onChange={this.handleInputChange.bind(
                                        this,
                                        'passport'
                                    )}
                                    maxLength={32}
                                    style={{ width: 287 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                </Row>

                <FormItem colon={false} label="联系方式" {...oneCol}>
                    {getFieldDecorator('contactNo')(
                        <Input
                            type="text"
                            placeholder="请输入"
                            autoComplete="off"
                            onChange={this.handleInputChange.bind(
                                this,
                                'contactNo'
                            )}
                            maxLength={32}
                            style={{ width: 660, marginLeft: 10 }}
                        />
                    )}
                </FormItem>

                <FormItem colon={false} label="最后位置" {...oneCol}>
                    {getFieldDecorator('latestLocation')(
                        <Input
                            type="text"
                            placeholder="请输入"
                            autoComplete="off"
                            onChange={this.handleInputChange.bind(
                                this,
                                'latestLocation'
                            )}
                            maxLength={32}
                            style={{ width: 660, marginLeft: 10 }}
                        />
                    )}
                </FormItem>
            </div>
        )
        return renderDom
    }
}

export default FeedBack
