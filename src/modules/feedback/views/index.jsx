import React from 'react'
import FeedBack from './feedback'
import Transport from './transport'
import Other from './other'
import { Form, Button, Card, Message, Drawer } from 'antd'

import style from './index.less'

import * as Fetch from '../api'

class FeedBackIndex extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isValid: 1,
            isRisk: 1,
            transportInfo: [],
            visible: false
        }
    }

    // 关闭抽屉
    onClose = () => {
        this.resetPage()
        this.setState(
            {
                isValid: 1,
                isRisk: 1
            },
            () => {
                this.getConclusionList(true, true)
                this.props.onClose()
            }
        )
    }

    resetPage() {
        const { resetFields } = this.props.form
        resetFields()
    }

    getTransArrResult(infoArr) {
        this.setState({
            transportInfo: infoArr
        })
    }

    // 改变单选按钮
    handleRadioChange(type, e) {
        let isValid = this.state.isValid
        let isRisk = this.state.isRisk
        if (type === 'valid') {
            isValid = e.target.value
            isRisk = 1
        }
        if (type === 'risk') {
            isRisk = e.target.value
        }

        this.setState({
            isValid,
            isRisk
        })
        // 请求下拉列表
        this.getConclusionList(isValid, isRisk)
    }

    // 设置反馈下拉值
    getConclusionList(isValid, isRisk) {
        // 反馈结论
        let fbParam = []
        let type = ''

        if (isValid) {
            if (isRisk) {
                // 有风险的
                fbParam = [
                    {
                        type: 'feedbackoption', // 反馈选项
                        code: 'risk'
                    }
                ]
                type = 'risk'
                // 发请求
                Fetch.selectConfusionRiskList(fbParam)
                    .then(res => {
                        if (res.data) {
                            if (res.data.data.code === type) {
                                const resultList = res.data.data.result
                                this.setState({
                                    resultList
                                })
                            }
                        } else {
                            console.log('error')
                        }
                    })
                    .catch(e => {
                        console.log(e)
                    })
            } else {
                // 无风险的，清空结果列表
                this.setState({
                    resultList: []
                })
            }
        } else {
            fbParam = [
                {
                    type: 'feedbackoption', // 反馈选项
                    code: 'invalid'
                }
            ]
            type = 'invalid'

            // 发请求
            Fetch.selectConfusionInvalidList(fbParam)
                .then(res => {
                    if (res.data) {
                        if (res.data.data.code === type) {
                            const resultList = res.data.data.result
                            this.setState({
                                resultList
                            })
                        }
                    } else {
                        console.log('error')
                    }
                })
                .catch(e => {
                    console.log(e)
                })
        }
    }

    componentDidMount() {
        // 交通方式
        const transParam = [
            {
                type: 'feedbackoption', // 反馈选项
                code: 'transport' // 交通方式
            }
        ]

        // 发请求
        Fetch.selectTransList(transParam)
            .then(res => {
                if (res.data) {
                    if (res.data.data.code === 'transport') {
                        const transportSelectList = res.data.data.result
                        this.setState({
                            transportSelectList
                        })
                    }
                } else {
                    console.log('error')
                }
            })
            .catch(e => {
                console.log(e)
            })

        // 反馈结论， 默认有效，有风险
        this.getConclusionList(true, true)
    }

    // 提交表单
    submitForm(e) {
        e.preventDefault()
        const { validateFields } = this.props.form
        validateFields(err => {
            if (err) return
            this.submit()
        })
    }

    submit = () => {
        const self = this
        const { getFieldsValue } = this.props.form
        const { objectValue, objectType, taskType } = this.props

        const {
            conclusion = '',
            nation,
            passport,
            contactNo,
            latestLocation,
            valid,
            risk
        } = getFieldsValue()
        const param = {
            objectType, // 目前写死
            objectValue, // 身份证号
            taskType, // 任务来源，填"simple_warning，simple_losttrail,simple_monitor,simple_discovery"
            basicInfo: {
                // 基本信息，选填
                nation, // 国籍
                passport, // 护照号
                contactNo, // 联系方式
                latestLocation // 最后位置
            },
            transportInfo: this.state.transportInfo,
            conclusion: conclusion, // 反馈结论
            risk: risk, // 是否风险
            valid: valid
        } // 线索是否有效

        // 发请求
        Fetch.submitFeedBack(param)
            .then(res => {
                if (res.success) {
                    self.onClose()
                    Message.success('反馈成功')
                } else {
                    console.log('error')
                }
            })
            .catch(e => {
                console.log(e)
            })
    }

    static getDerivedStateFromProps(props, state) {
        if (props.visible !== state.visible) {
            return {
                visible: props.visible,
                isValid: 1,
                isRisk: 1
            }
        } else {
            return null
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.visible !== this.state.visible) {
            if (!this.props.visible) {
                this.resetPage()
            }
        }
    }

    render() {
        const { visible } = this.props
        const formParam = {
            formObj: this.props.form,
            visible: visible
        }

        const feedbackForm = (
            <div className={style.drawerContent}>
                <Form className={style.cards}>
                    <Card
                        className={style.feedbackInfo}
                        title="反馈信息"
                        style={{ width: 778 }}
                    >
                        <FeedBack {...formParam} />
                    </Card>
                    <div>
                        <Transport
                            {...formParam}
                            transportSelectList={this.state.transportSelectList}
                            getTransArrResult={arr => {
                                this.getTransArrResult(arr)
                            }}
                        />
                    </div>
                    <Card
                        className={style.other}
                        title="其他信息"
                        style={{ width: 778 }}
                    >
                        <Other
                            {...formParam}
                            isValid={this.state.isValid}
                            isRisk={this.state.isRisk}
                            resultList={this.state.resultList}
                            changeRadio={this.handleRadioChange.bind(this)}
                        />
                    </Card>
                </Form>
            </div>
        )
        return (
            <Drawer
                title="详情"
                width={820}
                closable={true}
                visible={this.state.visible}
                onClose={this.props.onClose}
            >
                <div style={{ position: 'relative', height: '100%' }}>
                    {feedbackForm}
                    <div className="drawerBtn">
                        <Button
                            onClick={this.onClose}
                            style={{ marginRight: 8 }}
                        >
                            取消
                        </Button>
                        <Button
                            onClick={this.submitForm.bind(this)}
                            type="primary"
                        >
                            确定
                        </Button>
                    </div>
                </div>
            </Drawer>
        )
    }
}

export default Form.create()(FeedBackIndex)
