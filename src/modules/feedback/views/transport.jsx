import React from 'react'
import { Form, Row, Col, Input, DatePicker, Card, Select } from 'antd'
import shortid from 'shortid'
import moment from 'moment'
import style from './transport.less'
const FormItem = Form.Item
const Option = Select.Option

const traficNameMap = {
    // 交通方式对应的交通标识
    '1': '列车班次', // 火车
    '2': '航班号', // 飞机
    '3': '车牌', // 大巴
    '4': '车牌', // 私家车
    '5': '交通标识' // 其它
}

class Transport extends React.Component {
    constructor(props) {
        super(props)
        this.transItem = {
            id: 'transId1',
            direction: 'in',
            transport: '1',
            numberName: '列车班次',
            number: '',
            departureTime: moment('00:00:00', 'HH:mm:ss').format(
                'YYYY-MM-DD HH:mm:ss'
            ),
            departureAddr: '',
            arriveTime: moment('23:59:59', 'HH:mm:ss').format(
                'YYYY-MM-DD HH:mm:ss'
            ),
            arriveAddr: ''
        }
        this.state = {
            visible: false,
            transArr: [Object.assign({}, this.transItem)]
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (props.visible !== state.visible) {
            return {
                visible: props.visible,
                transArr: [
                    Object.assign(
                        {},
                        {
                            id: 'transId1',
                            direction: 'in',
                            transport: '1',
                            numberName: '列车班次',
                            number: '',
                            departureTime: moment(
                                '00:00:00',
                                'HH:mm:ss'
                            ).format('YYYY-MM-DD HH:mm:ss'),
                            departureAddr: '',
                            arriveTime: moment('23:59:59', 'HH:mm:ss').format(
                                'YYYY-MM-DD HH:mm:ss'
                            ),
                            arriveAddr: ''
                        }
                    )
                ]
            }
        } else {
            return null
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.visible !== this.state.visible) {
            if (this.props.visible) {
                this.setState({
                    transArr: this.state.transArr
                })
            }
        }
    }

    // 公共方法，子组件搜集数据，通过回调函数传给父组件
    updateTransArrData() {
        const { transArr } = this.state

        const transportInfo = []
        for (const item of transArr) {
            const { direction, transport, number } = item
            const departureTime = item.departureTime
            const departureAddr = item.departureAddr
            const arriveTime = item.arriveTime
            const arriveAddr = item.arriveAddr
            if (direction === 'in') {
                transportInfo.push({
                    direction,
                    transport,
                    number,
                    departureAddr,
                    arriveTime
                })
            } else if (direction === 'out') {
                transportInfo.push({
                    direction,
                    transport,
                    number,
                    departureTime,
                    arriveAddr
                })
            }
        }
        this.props.getTransArrResult(transportInfo)
    }

    // add交通
    addTransForm() {
        const { transArr } = this.state
        const nextId = shortid.generate()
        const newTransArr = [...transArr]
        const newItem = Object.assign({}, this.transItem, { id: nextId })
        newTransArr.push(newItem)

        this.setState(
            {
                transArr: newTransArr
            },
            () => {
                this.updateTransArrData()
            }
        )
    }

    // 删除交通方式
    deleteTraffic(id) {
        const { transArr } = this.state
        const newTransArr = [...transArr]
        for (const index in newTransArr) {
            if (id === newTransArr[index].id) {
                newTransArr.splice(index, 1)
            }
        }
        this.setState(
            {
                transArr: newTransArr
            },
            () => {
                this.updateTransArrData()
            }
        )
    }

    // 每个交通框中的内容修改时，设置到transArr中。
    setTransFormItem(type, id, value) {
        console.log(type, id, value)
        const { transArr } = this.state
        const { formObj } = this.props
        for (const i in transArr) {
            if (id === transArr[i].id) {
                transArr[i][type] = value
                if (type === 'transport') {
                    transArr[i]['numberName'] =
                        traficNameMap[value] || '交通标识'
                    // 改变下拉选项，初始化文本框
                    transArr[i]['number'] = ''
                    formObj.setFieldsValue({
                        [`number-${id}`]: ''
                    })
                }
                this.setState(
                    {
                        transArr
                    },
                    () => {
                        this.updateTransArrData()
                    }
                )
            }
        }
    }

    // 日期选择
    handleDateChange(type, id, momentObj, dateString) {
        // 到达时间比出发时间晚，规则。
        this.setTransFormItem(type, id, dateString)
    }

    // 日期ok
    handleDateOk(type, id, momentObj) {
        const value = momentObj.format('YYYY-MM-DD HH:mm:ss')
        this.setTransFormItem(type, id, value)
    }

    // 输入值改变时
    handleInputChange(type, id, e) {
        this.setTransFormItem(type, id, e.target.value)
    }

    newTransDom(item, index) {
        const { formObj, transportSelectList } = this.props
        const { getFieldDecorator } = formObj

        const formItemLayout = {
            labelCol: { span: 5 },
            wrapperCol: { span: 18, offset: 1 }
        }

        const curDirection = item.direction

        return (
            <div className={style.transport}>
                <Row>
                    <Col span={12}>
                        <FormItem
                            {...formItemLayout}
                            colon={false}
                            label={'出入方式'}
                        >
                            {getFieldDecorator(`direction-${item.id}`, {
                                initialValue: 'in',
                                rules: [
                                    {
                                        required: true,
                                        message: '出入方式不能为空'
                                    }
                                ]
                            })(
                                <Select
                                    allowClear={true}
                                    placeholder="请选择"
                                    onChange={this.setTransFormItem.bind(
                                        this,
                                        'direction',
                                        item.id
                                    )}
                                    style={{ width: 260 }}
                                >
                                    <Option key="in" value="in">
                                        来辖
                                    </Option>
                                    <Option key="out" value="out">
                                        计划出辖
                                    </Option>
                                </Select>
                            )}
                        </FormItem>
                    </Col>
                    <Col span={12}>
                        {index > 0 ? (
                            <a onClick={this.deleteTraffic.bind(this, item.id)}>
                                删除交通方式
                            </a>
                        ) : null}
                    </Col>
                </Row>
                <Row>
                    <Col span={12}>
                        <FormItem
                            {...formItemLayout}
                            colon={false}
                            label={`交通方式${index + 1}`}
                        >
                            {getFieldDecorator(`transport-${item.id}`, {
                                initialValue: '1',
                                rules: [
                                    {
                                        required: true,
                                        message: '交通方式不能为空'
                                    }
                                ]
                            })(
                                <Select
                                    id="J_trans"
                                    allowClear={true}
                                    placeholder="请选择"
                                    onChange={this.setTransFormItem.bind(
                                        this,
                                        'transport',
                                        item.id
                                    )}
                                    style={{ width: 260 }}
                                >
                                    {transportSelectList &&
                                        transportSelectList.map(
                                            (itemS, indexS) => {
                                                return (
                                                    <Option
                                                        key={indexS}
                                                        value={itemS.order}
                                                    >
                                                        {itemS.value}
                                                    </Option>
                                                )
                                            }
                                        )}
                                </Select>
                            )}
                        </FormItem>
                    </Col>
                    <Col span={12}>
                        <FormItem
                            {...formItemLayout}
                            colon={false}
                            label={item.numberName}
                        >
                            {getFieldDecorator(`number-${item.id}`)(
                                <Input
                                    type="text"
                                    placeholder="请输入"
                                    autoComplete="off"
                                    onChange={this.handleInputChange.bind(
                                        this,
                                        'number',
                                        item.id
                                    )}
                                    maxLength={32}
                                    style={{ width: 260 }}
                                />
                            )}
                        </FormItem>
                    </Col>
                </Row>
                {curDirection === 'in' ? (
                    <Row>
                        <Col span={12}>
                            <FormItem
                                {...formItemLayout}
                                colon={false}
                                label="到达时间"
                            >
                                {getFieldDecorator(`arriveTime-${item.id}`, {
                                    initialValue: moment('23:59:59', 'HH:mm:ss')
                                })(
                                    <DatePicker
                                        style={{ width: 260 }}
                                        placeholder="请输入"
                                        onChange={this.handleDateChange.bind(
                                            this,
                                            'arriveTime',
                                            item.id
                                        )}
                                        onOk={this.handleDateOk.bind(
                                            this,
                                            'arriveTime',
                                            item.id
                                        )}
                                        showTime={{
                                            hideDisabledOptions: true
                                        }}
                                        format="YYYY-MM-DD HH:mm:ss"
                                        showToday={false}
                                    />
                                )}
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem
                                {...formItemLayout}
                                colon={false}
                                label="出发地"
                                style={{ paddingleft: 3 }}
                            >
                                {getFieldDecorator(`departureAddr-${item.id}`)(
                                    <Input
                                        type="text"
                                        placeholder="请输入"
                                        autoComplete="off"
                                        onChange={this.handleInputChange.bind(
                                            this,
                                            'departureAddr',
                                            item.id
                                        )}
                                        maxLength={32}
                                        style={{ width: 260 }}
                                    />
                                )}
                            </FormItem>
                        </Col>
                    </Row>
                ) : null}
                {curDirection === 'out' ? (
                    <Row>
                        <Col span={12}>
                            <FormItem
                                {...formItemLayout}
                                colon={false}
                                label="出发时间"
                            >
                                {getFieldDecorator(`departureTime-${item.id}`, {
                                    initialValue: moment('00:00:00', 'HH:mm:ss')
                                })(
                                    <DatePicker
                                        style={{ width: 260 }}
                                        placeholder="请输入"
                                        onChange={this.handleDateChange.bind(
                                            this,
                                            'departureTime',
                                            item.id
                                        )}
                                        onOk={this.handleDateOk.bind(
                                            this,
                                            'departureTime',
                                            item.id
                                        )}
                                        showTime={{
                                            hideDisabledOptions: true
                                        }}
                                        format="YYYY-MM-DD HH:mm:ss"
                                        showToday={false}
                                    />
                                )}
                            </FormItem>
                        </Col>
                        <Col span={12}>
                            <FormItem
                                {...formItemLayout}
                                colon={false}
                                label="到达地"
                            >
                                {getFieldDecorator(`arriveAddr-${item.id}`)(
                                    <Input
                                        type="text"
                                        placeholder="请输入"
                                        autoComplete="off"
                                        onChange={this.handleInputChange.bind(
                                            this,
                                            'arriveAddr',
                                            item.id
                                        )}
                                        maxLength={32}
                                        style={{ width: 260 }}
                                    />
                                )}
                            </FormItem>
                        </Col>
                    </Row>
                ) : null}
            </div>
        )
    }

    render() {
        const renderDom = (
            <Card
                className={style.trans}
                title="交通信息"
                extra={
                    <a
                        onClick={this.addTransForm.bind(this)}
                        className={style.newTrans}
                    >
                        + 新增交通
                    </a>
                }
                style={{ width: 778 }}
            >
                {this.state.transArr.map((item, index) => {
                    return (
                        <div key={item.id} index={index}>
                            {this.newTransDom(item, index)}
                        </div>
                    )
                })}
            </Card>
        )
        return renderDom
    }
}

export default Transport
