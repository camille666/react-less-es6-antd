import axios from 'axios'

export async function submitFeedBack(params) {
    return axios.post(`/api/feedback`, { params: params })
}

// 无效
export async function selectConfusionInvalidList(params) {
    return axios.get(`/api/getInvalidList`, { params: params })
}

// 反馈结论下拉框
export async function selectConfusionRiskList(params) {
    return axios.get(`/api/getRiskList`, { params: params })
}

// 交通方式
export async function selectTransList(params) {
    return axios.get(`/api/getTransportList`, { params: params })
}
