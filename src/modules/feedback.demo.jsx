import React from 'react'
import Feedback from './feedback/views/index.jsx'
import { Table, Tag } from 'antd'

import style from './feedback.demo.less'

const data = [
    {
        key: '1',
        name: '周红',
        age: 31,
        address: '江西',
        tags: ['善良', '努力', '嫉妒', '欺上瞒下']
    },
    {
        key: '2',
        name: '顾少华',
        age: 36,
        address: '深圳',
        tags: ['狡诈', '庸俗', '自以为是']
    },
    {
        key: '3',
        name: '刘旭胖哥',
        age: 29,
        address: '北京，杭州',
        tags: ['小聪明', '投机取巧', '小心眼', '心胸狭窄', '嫉妒', '欺上瞒下']
    },
    {
        key: '4',
        name: '郁辰磊',
        age: 34,
        address: '上海',
        tags: ['油腻', '装逼', '自以为是', '小心眼', '心胸狭窄']
    },
    {
        key: '5',
        name: '曹张文',
        age: 33,
        address: '北京，杭州',
        tags: ['善良', '粗俗', '自以为是']
    },
    {
        key: '6',
        name: '叶静俊',
        age: 40,
        address: '杭州',
        tags: ['聪明', '温和', '见识少']
    },
    {
        key: '7',
        name: '任明明',
        age: 32,
        address: '滨江',
        tags: ['努力', '迟顿', '投机取巧', '嫉妒', '欺上瞒下']
    },
    {
        key: '8',
        name: '金炜卿',
        age: 29,
        address: '北京，杭州',
        tags: ['小聪明', '急躁', '见识少', '投机取巧', '嫉妒', '欺上瞒下']
    },
    {
        key: '9',
        name: '李华',
        age: 38,
        address: '杭州，西安',
        tags: ['善良', '自以为是']
    },
    {
        key: '10',
        name: '赵英超',
        age: 31,
        address: '杭州',
        tags: ['温和', '谦逊']
    }
]

export default class FeedbackApp extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            objectType: '', // 目前写死
            objectValue: '', // 身份证号
            taskType: '' // 任务来源，填"simple_warning，simple_losttrail,simple_monitor,simple_discovery"
        }
    }

    // 显示drawer
    showDrawer = () => {
        this.setState({
            visible: true,
            objectType: '101001', // 目前写死
            objectValue: '650102197309301159', // 身份证号
            taskType: 'simple_warning' // 任务来源，填"simple_warning，simple_losttrail，simple_monitor,simple_discovery"
        })
    }

    onCloseDrawer = () => {
        this.setState({
            visible: false
        })
    }

    render() {
        const columns = [
            {
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
                render: text => <a>{text}</a>
            },
            {
                title: '年龄',
                dataIndex: 'age',
                key: 'age'
            },
            {
                title: '地址',
                dataIndex: 'address',
                key: 'address'
            },
            {
                title: '标签',
                key: 'tags',
                dataIndex: 'tags',
                render: tags => (
                    <span>
                        {tags.map(tag => (
                            <Tag color="blue" key={tag}>
                                {tag}
                            </Tag>
                        ))}
                    </span>
                )
            },
            {
                title: '动作',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <a style={{ paddingRight: '10px' }}>{record.name}</a>
                        <a onClick={this.showDrawer.bind(this)}>打开抽屉</a>
                    </span>
                )
            }
        ]

        return (
            <div className={style.drawerBox}>
                <Table
                    columns={columns}
                    dataSource={data}
                    pagination={false}
                    emptyIconStyle={{ fontSize: '100px' }}
                />

                <Feedback
                    visible={this.state.visible}
                    onClose={this.onCloseDrawer}
                    objectType={this.state.objectType}
                    objectValue={this.state.objectValue}
                    taskType={this.state.taskType}
                />
            </div>
        )
    }
}
